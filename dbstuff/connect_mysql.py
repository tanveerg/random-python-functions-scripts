import base64
import os

import boto3
import sqlalchemy


BASE_CONNECTION_STRING = 'mysql+pymysql://{db_user}:{db_pwd}@{db_host}:{db_port}/{db_name}?charset=utf8mb4'


def get_regional_client(name, endpoint_url=None):
    if endpoint_url:
        return boto3.client(
            name,
            endpoint_url=endpoint_url,
            region_name=os.environ['AWS_STACK_REGION'])
    else:
        return boto3.client(name, region_name=os.environ['AWS_STACK_REGION'])


def decrypt(encrypted):
    kms = get_regional_client('kms')
    return kms.decrypt(
        CiphertextBlob=base64.decodebytes(
            encrypted.encode('utf-8').decode('unicode-escape').encode()))['Plaintext'].decode()


def create_engine():
    processing_threads = int(os.environ['NUM_THREADS'])
    pwd = os.environ['DB_PASSWORD_ENCRYPTED']
    engine = sqlalchemy.create_engine(BASE_CONNECTION_STRING.format(
            db_user=os.environ["DB_USER"],
            db_pwd=pwd,
            db_host=os.environ['DB_HOST'],
            db_port=os.environ['DB_PORT'],
            db_name=os.environ['DB_NAME']
        ),
        pool_size=processing_threads // 2 if processing_threads > 1 else 1,
        max_overflow=0
    )
    return engine
