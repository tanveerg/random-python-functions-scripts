import copy
import functools


class Vividict(dict):
    """Modified dictionary class to facilitate
    dynamic creation of nested dictionary"""
    def __missing__(self, key):
        value = self[key] = type(self)()
        return value


def nested_dict_get(nested_dict, keys):
    """Traverse and return nested dictionary element
    
    :param nested_dict: Vividict
    :param keys: List
    
    :rtype: Dict or String
    """
    return functools.reduce(lambda d, k: d[k], keys, nested_dict)


def nested_dict_set(nested_dict, keys, value):
    """Sets the value for a nested dictionary element
    
    :param nested_dict: Vividict object
    :param keys: List
    :param value: Dict or String
    """
    nested_dict_get(nested_dict, keys[:-1])[keys[-1]] = value


def create_nested_dict(nested_dict, keys, payload):
    """Creates nested dictionary
    
    :param nested_dict: Vividict object
    :param keys: List
    
    :param payload: Dict or String
    """
    if not nested_dict_get(nested_dict, keys[:-1]):
        nested_dict_set(nested_dict, keys[:-1], payload)
        
        
def generate_schema_dict(attr_list, attr_dict):
    """Generates dynamic dictionary based on text input list
    which has elements like "key1.key2.value12"
    
    :param attr_list: List
    :param attr_dict: Dict
    
    :rtype: Vividict object
    """
    d = Vividict()

    for attr in attr_list:
        create_nested_dict(
            d,
            attr.split('.'),
            attr_dict[attr])

    return d
    

def fetch_children_keys(field_name, struct_list, split_pat='.children.'):
    """Return list of children structure keys
    
    :param field_name: String
    :param struct_list: List
    :param split_pat: String - Splitting pattern. Default - '.children.'
    
    :rtype: List
    """
    ret_list = []
    num_children = field_name.count(split_pat)

    for a in range(0, num_children):
        index_num = a + 1
        split_str = field_name.split(split_pat, index_num)[:index_num]

        pat_join = split_pat.join(split_str)
        pat_join_nullable = '{}.children'.format(pat_join)

        if pat_join_nullable not in struct_list:
            ret_list.append(pat_join_nullable)

    return ret_list
    
    
def getFromDict(dataDict, mapList):
    """Fetches the value from nested dict based
    on the passed in mapList
    
    :param dataDict: Dict
    :param mapList: List
    
    :rtype: String or Dict
    """
    return functools.reduce(operator.getitem, mapList, dataDict)


def setInDict(dataDict, mapList, value):
    """Sets value in the nested dict based on the
    passed in mapList
    
    :param dataDict: Dict
    :param mapList: List
    
    :param value: String or Dict
    """
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value


def convert_back_to_dict(list_attrs, input_dict):
    """Converts instances of Vividict object to
    standard dictionary
    
    :param list_attrs: List
    :param input_dict: Vividict object
    
    :rtype: Dict
    """
    mod_dict = copy.deepcopy(dict(input_dict))
    struct_list = []
    for l in list_attrs:
        effective_key = l[:-2]
        if 'children' in effective_key:
            struct_list.extend(fetch_children_keys(effective_key, struct_list))

    for s in struct_list:
        map_list = s.split('.')
        original_children_struct = getFromDict(mod_dict, map_list)
        setInDict(mod_dict, map_list, dict(original_children_struct))

        poc_elements = s[:-9].split('.')
        original_poc_struct = getFromDict(mod_dict, poc_elements)
        setInDict(mod_dict, poc_elements, dict(original_poc_struct))

    return mod_dict

    