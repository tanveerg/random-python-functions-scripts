import logging
import socket
import ssl
import time


LOGGER = logging.getLogger(__name__)
LOGGING_FORMAT = "%(asctime)s %(levelname)-5.5s " \
                 "[%(name)s]:[%(threadName)s] " \
                 "%(message)s"


TIME_BETWEEN_RETRY = 1
"""Seconds between retry attempt"""


def check_connection(sock, host, port, max_conn_retries, ssl_mode=False):
    """Check socket connection for the host and port

    :param sock: socket.socket object
    :param host: String
    :param port: String
    :param max_conn_retries: Integer
    :param ssl_mode: Boolean

    :raises: ConnectionError
    """
    if ssl_mode:
        LOGGER.info("Will be checking ssl connection")
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        sock = ssl_context.wrap_socket(sock, server_hostname=host)

    retry_count = 0
    while retry_count < max_conn_retries:
        LOGGER.info(f"Retry number: {retry_count} of {max_conn_retries}")
        try:
            sock.connect((host, int(port)))
            sock.close()
            return
        except socket.error:
            time.sleep(TIME_BETWEEN_RETRY)
            retry_count += 1

    raise ConnectionError(f"Unable to establish connection "
                          f"with host: {host} and port: {port}")
