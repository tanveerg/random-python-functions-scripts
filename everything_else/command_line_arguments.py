import argparse


def parse_cli_arguments():
    """Parse command line arguments

    :rtype - argparse.Namespace
    """
    parser = argparse.ArgumentParser(description='my_cli_app')
    parser.add_argument('--start_date', type=str, required=True,
                        help='Start date')
    parser.add_argument('--end_date', type=str, required=True,
                        help='End date')
    parser.add_argument('-v', '--verbose', action='store_true')

    return parser.parse_args()
