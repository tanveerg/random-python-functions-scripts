import logging


def silence_noisy_loggers():
    """Some things are noisier than others. Some libraries mothers are noisier
    than other libraries mothers.
    """
    for logger in ['boto3', 'botocore',
                   'botocore.vendored.requests.packages.urllib3']:
        logging.getLogger(logger).setLevel(logging.WARNING)
