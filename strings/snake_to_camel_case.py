def _snake_to_camel_case(column_name):
    """Changes snake case to camel case

    :param column_name: String

    :rtype: String
    """
    split_str = column_name.split('_')
    ret_str = ''
    count = 0
    for s in split_str:
        count += 1
        if count == 1:
            ret_str = '{}{}'.format(ret_str, s)
        else:
            ret_str = '{}{}'.format(ret_str, s.title())
    return ret_str
