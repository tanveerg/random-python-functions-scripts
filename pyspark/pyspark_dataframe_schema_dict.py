from pyspark.sql.types import *

def _get_schema_dict(schema):
    """Fetches a dictionary of column name and
    its structfield schema
    
    :param schema: pyspark.sql.types.StructType 
                   (schema of the data frame)
    
    :rtype: Dict
    """
    expected_schema_dict = {}
    for field in schema:
        expected_schema_dict[field.name] = field
    return expected_schema_dict
