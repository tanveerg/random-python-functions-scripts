from pyspark.sql.session import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import col, lit, udf, lower, when

def string_to_array(value, element_type):
    """Convert string literal to array

    :param value:
    :param element_type:

    :rtype:
    """
    ret_list = []
    type_cast = str
    if isinstance(element_type, LongType):
        type_cast = long
    if value:
        split_str = value.split(',')
        for s in split_str:
            if s:
                ret_list.append(type_cast(s))
        return ret_list
    else:
        return None


def resolve_choice_types(df, field_name, correct_type=ArrayType(StringType())):
    """Converts strings to array types

    :param df: Pyspark Dataframe
    :param field_name: String
    :param correct_type: pyspark.sql.types object.
                         Default - ArrayType(StringType())

    :rtype: Pyspark Dataframe
    """
    mod_column_name = '{}2'.format(field_name)
    df = df.withColumnRenamed(field_name, mod_column_name)

    resolve_choice_udf = udf(
        lambda x: string_to_array(x, correct_type.elementType),
        correct_type)
    mod_df = df.withColumn(
        field_name,
        resolve_choice_udf(mod_column_name))

    return mod_df
