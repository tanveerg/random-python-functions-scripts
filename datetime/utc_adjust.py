import datetime

# Obtain the UTC Offset for the current system:
UTC_OFFSET_TIMEDELTA = datetime.datetime.utcnow() - datetime.datetime.now()
local_datetime = datetime.datetime.strptime("2008-09-17 14:04:00", "%Y-%m-%d %H:%M:%S")
result_utc_datetime = local_datetime + UTC_OFFSET_TIMEDELTA
result_utc_datetime.strftime("%Y-%m-%d %H:%M:%S")
# '2008-09-17 04:04:00'
