import datetime

def get_year_month_day(input_date):
    """Extracts year, month and day from
    a datetime object of format YYYY-MM-DD

    :param input_date: Datetime object

    :rtype: tuple<string, string, string>
    """
    split_date = input_date.strftime('%Y-%m-%d').split('-')
    return split_date[0], split_date[1], split_date[2]
