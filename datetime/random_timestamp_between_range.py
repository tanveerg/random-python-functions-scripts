import datetime
import random


def _random_timestamp_between_range(timestamp):
    """generates a random timestamp in a given range

    :param timestamp: Integer (unix timestmap)

    :rtype: Integer (unix timestamp)
    """
    current_datetime = datetime.datetime.fromtimestamp(timestamp/1000)
    range_start_datetime = current_datetime - datetime.timedelta(days=20)
    return random.randrange(
        int(range_start_datetime.timestamp() * 1000),
        timestamp
    )
