import datetime


def date_range(start_date, end_date):
    """ Return a range of dates based on the passed
    in start_date and end_date

    :param start_date: Datetime object
    :param end_date: Datetime object
    
    :rtype: List
    """
    ret_list = []
    delta = end_date - start_date
    for i in range(delta.days + 1):
        ret_list.append(start_date + datetime.timedelta(i))
    return ret_list
