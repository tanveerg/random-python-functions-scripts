import argparse
import logging
import time

import boto3
import pandas as pd


LOGGER = logging.getLogger(__name__)
LOGGING_FORMAT = '%(asctime)s %(levelname)s %(message)s'

SERVICE_URL_MAP = {
    'dynamodb': 'http://localhost:8000',
}


def _get_boto3_client(service_name, args):
    """Returns boto3 client for the requested service_name

    :param service_name: String
    :param args: CLI arguments

    :rtype: boto3.Client object
    """
    if args.local:
        LOGGER.debug('attempting to fetch local %s client', service_name)
        return boto3.client(
            service_name,
            endpoint_url=SERVICE_URL_MAP[service_name])
    else:
        return boto3.client(service_name)


def _extract_key_and_type(column):
    """Extracts key name and key type for dynamodb payload

    :param column: String

    :rtype: String
    """
    split_str = column.split(' ')
    return split_str[0], split_str[1].replace('(', '').replace(')', '')


def prepare_dynamodb_item_payload(row, column_names):
    """Prepare the item payload for dynamodb

    :param row: Pandas.series (row)
    :param column_names: List

    :rtype: Dict
    """
    payload = {}
    for column in column_names:
        key, key_type = _extract_key_and_type(column)
        value = row[column]
        if str(value) != 'nan':
            payload[key] = {key_type: str(row[column])}
    return payload


def reload_dynamodb_from_expport(file_path, ddb_client, args):
    """Reloads dynamodb from csv export

    :param file_path: String - path to backup file
    :param ddb_client: boto3.Client Object (dynamodb)
    :param args: CLI Arguments
    """
    input_df = pd.read_csv(file_path, sep=',')
    column_names = input_df.columns.values
    count = 0
    for index, row in input_df.iterrows():
        count += 1
        payload = prepare_dynamodb_item_payload(row, column_names)
        if count < 22:
            LOGGER.info('record index - %i', count)
            response = ddb_client.put_item(
                TableName=args.table, Item=payload)
            if response.get('ResponseMetadata').get('HTTPStatusCode') != 200:
                LOGGER.error('failed to put_item to the dynamodb table')


def _parse_cli_arguments():
    """Parse command line arguments

    :rtype - argparse.Namespace
    """
    parser = argparse.ArgumentParser(description='trigger_events')
    parser.add_argument('-f', '--file', type=str, required=True,
                        help='path to the export file')
    parser.add_argument('-t', '--table', type=str, required=True,
                        help='name of dynamodb table')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='for more verbose log output')
    parser.add_argument('-l', '--local', action='store_true',
                        help='local dynamodb endpoint')
    return parser.parse_args()


def _silence_noisy_loggers():
    """Some things are noisier than others. Some libraries mothers are noisier
    than other libraries mothers.
    """
    for logger in ['boto3', 'botocore',
                   'botocore.vendored.requests.packages.urllib3']:
        logging.getLogger(logger).setLevel(logging.WARNING)


def main():
    """What executes when the script is fun"""
    start = time.time()
    args = _parse_cli_arguments()
    level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=level, format=LOGGING_FORMAT)
    _silence_noisy_loggers()

    LOGGER.info('Fetching dynamodb client')
    ddb_client = _get_boto3_client('dynamodb', args)

    input_file = args.file
    LOGGER.info('Attempting to load data from export file: %s',
                input_file)
    reload_dynamodb_from_expport(input_file, ddb_client, args)

    LOGGER.info('total time elapsed: %f seconds',
                time.time() - start)


if __name__ == '__main__':
    main()
