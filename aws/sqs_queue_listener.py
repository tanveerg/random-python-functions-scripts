import json

import boto3

from pprint import pprint


SQS_CLIENT = boto3.resource('sqs', endpoint_url='http://localhost:4567')

QUEUE_URL = 'http://localhost:4567/queue/myqueue'


def listen_on_queue(queue_url):
    queue = SQS_CLIENT.Queue(queue_url)
    while True:
        messages = queue.receive_messages(MaxNumberOfMessages=10)
        if len(messages) > 0:
            for message in messages:
                yield message


def main():
    while True:
        for msg in listen_on_queue(QUEUE_URL):
            deserialized = json.loads(msg.body)
            pprint(deserialized)


if __name__ == '__main__':
    main()
